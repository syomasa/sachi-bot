import discord
import json
import os
import asyncio
import itertools
from datetime import datetime as dt
from discord.ext import commands, tasks
#from modules.sheet import Sheet
from dotenv import load_dotenv

load_dotenv() # Load env variables

bot = commands.Bot(command_prefix='>')
img_cycle = itertools.cycle(os.listdir(os.getenv("IMG_PATH")))

@bot.event
async def on_ready():
    try:
        # check_birthday.start() # start check_birthday loop
        # print('check_birthday launched successfully')
        # check_leaving_members.start() # start check_leaving_members loop
        # print('check_leaving_members launched successfully')
        set_banner.start()
        print("set_banner launched succesfully")
        update_server_time.start() #start update_server_time loop
        print('update_server_time launched successfully')
    except:
        # check_birthday.restart()
        # check_leaving_members.restart()
        update_server_time.restart()
        set_banner.restart()
        print('Due to unknown error all tasks are restarted')
    finally:
        print('Bot is online') # after loops have started print bot's status

@tasks.loop(seconds=60)
async def update_server_time():
    await bot.change_presence(activity=discord.Game(dt.utcnow().strftime('ST: %H:%M:%S'))) # change bot's gaming activity to show utc time

@tasks.loop(hours=12)
async def set_banner():
    img_path = f"{os.getenv('IMG_PATH')}/{next(img_cycle)}"
    # TODO: Make this generalized instead of hardcoding guild id
    guild = bot.get_guild(687350059142217784)
    with open(img_path, "rb") as img:
        try:
            await guild.edit(banner=img.read())
        except Exception as e:
            print("Ignoring exception following exception:\n\n", e)
    await asyncio.sleep(0.1)

# @tasks.loop(seconds=86400)
# async def check_birthday():
#     date_today = dt.utcnow().strftime('%m.%d')
#     birthdays, values = Sheet('Bunny Boilers', 'birthdays').get_sheet_data()
#     #print(values, date_today)
#     # start looping birthday through birthday data
#     for row in values:
#         if row[-1] == date_today:
#             for guild in bot.guilds:
#                 for channel in guild.channels:
#                     user = bot.get_user(int(row[0]))
#                     if 'announcements' in channel.name:
#                         await channel.send(f'@here Today is birthday of {user.mention}. Have fun partying whole day :partying_face::partying_face::partying_face::partying_face:')
#
#     await asyncio.sleep(0.001)
#
# @tasks.loop(seconds=86400)
# async def check_leaving_members():
#     '''check members that has left the server and remove them from database'''
#     sh = Sheet('Bunny Boilers', 'birthdays')
#     user_data, values = sh.get_sheet_data()
#     user_ids = []
#     for guild in bot.guilds:
#         for user in guild.members:
#             user_ids.append(user.id)
#
#     for i, row in enumerate(values, 1):
#         #print(row)
#         if (i < len(values)) and int(values[i][0]) not in user_ids:
#             sh.delete_user_information(int(values[i][0]))
#
#     await asyncio.sleep(0.001)

if __name__ == '__main__':
    # import cogs for discord bot
    dir_list = os.listdir('cogs')
    for file in dir_list:
        if file.endswith('.py'):
            file = file.strip('.py')
            bot.load_extension(f'cogs.{file}')
            print(f'{file} cog loaded successfully')

    # load api key
    KEY=os.getenv("BOT_TOKEN")
    bot.run(KEY)
