import discord
from discord.ext import commands

# Bot fetches images from imgur and downloads them into IMG_PATH
# IMG_PATH is defined at the start of this file

IMG_PATH = "images"

class BannerSwitcher(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="setalbum")
    async def set_album(self, ctx):
        pass

def setup(bot):
    bot.add_cog(BannerSwitcher(bot))
