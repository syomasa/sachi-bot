import discord
from discord.ext import commands
from modules.sheet import Sheet
from datetime import datetime as dt

class Birthday(commands.Cog):
    sh = Sheet('Bunny Boilers', 'birthdays')
    def __init__(self, bot):
        self.bot = bot # required by discord py

    @commands.command(name='bday.add')
    async def add(self, ctx, date:str):
        if '.' in date:
            date_listed = date.split('.')
            if (len(date_listed) == 2 ) and len(date_listed[0]) == 2 and len(date_listed[1]) == 2: # Dealing with user inputs hopefuly it works properly
                date_listed.reverse()
                date_database = f'{date_listed[0]}.{date_listed[1]}'
                if ctx.author.nick == None:
                    data_row = [str(ctx.author.id), ctx.author.name, str(date_database)] # information that is moved into sheet
                    self.sh.append_datarow(data_row)

                else:
                    data_row = [str(ctx.author.id), ctx.author.nick, str(date_database)]
                    self.sh.append_datarow(data_row)

                await ctx.send('Succesfully updated sheet data')
            else:
                await ctx.send('Date is in wrong format correct format is `dd.mm`')
        else:
            await ctx.send('Date is in wrong format correct format is `dd.mm`')

    @commands.command(name='bday.del')
    async def delete_information(self, ctx):
        self.sh.delete_user_information(ctx.author.id)
        await ctx.send(f'Successfully deleted user `{ctx.author.name}`\'s data')

    @commands.command(name='bday.upcoming')
    async def upcoming_birthdays(self, ctx):
        data, values = self.sh.get_sheet_data()
        if data:
            date_numbers = []
            date_today_num = int(dt.utcnow().strftime('%m%d'))
            for i, _ in enumerate(values, 1):
                if i < len(values):
                    date_value = int(values[i][-1].replace('.', ''))
                    if date_value > date_today_num:
                        date_numbers.append(date_value)

            upcoming_value = min(date_numbers)
            for i, _ in enumerate(values, 1):
                if (i < len(values)) and upcoming_value == int(values[i][-1].replace('.', '')):
                    reformat = values[i][-1].split('.')
                    date_format = f'{reformat[1]}.{reformat[0]}'
                    for guild in self.bot.guilds:
                        user = guild.get_member(int(values[i][0]))
                        if user.nick == None:
                            await ctx.send(f'Next birthday is `{user.name}`\'s on `{date_format}.`')

                        else:
                            await ctx.send(f'Next birthday is `{user.nick}`\'s on `{date_format}`')

        else:
            await ctx.send('No birthdays are given to me I feel lonely :cry:')

    @commands.command(name='bday.status')
    async def bday_status(self, ctx):
        ''' Checks if the user has birthday added '''
        column = self.sh.get_col_data(1)
        if str(ctx.author.id) in column:
            await ctx.send(f'User `{ctx.author.name}`\'s birthday is found from database ***uwu noises***')

        else:
            await ctx.send('Senpai please pour your hot birthday into me with your huge `>bday.add dd.mm`')

def setup(bot):
    bot.add_cog(Birthday(bot))
