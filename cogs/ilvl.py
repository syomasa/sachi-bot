import discord
from discord.ext import commands
from modules.sheet import Sheet

class Ilvl(commands.Cog):
    """
    Class for making raiding related commands, such as managing gear
    """
    sheet = Sheet("Bunny Boilers", "Raiding")

    def __init__(self, bot):
        self.bot = bot # required by discord.py

    @commands.command(name="ilvl.add")
    async def add_itemlvl(self, ctx, role:str, ilvl:str):
        data_row = Sheet.create_data_row(str(ctx.author.id), ctx.author.name, role, ilvl)
        roles = ["tank", "heal", "rdps", "mdps"]
        if (role not in roles):
            await ctx.send("That role doesn't exist acceptable role names are [tank, heal, rdps, mdps]")

        else:
            if (ctx.author.nick != None):
                data_row = Sheet.create_data_row(str(ctx.author.id), ctx.author.nick, role, ilvl)

            self.sheet.append_datarow(data_row)
            await ctx.send("Your gear data has been updated.")

    @commands.command(name="ilvl.show")
    async def show(self, ctx):
        rows = self.sheet.sortRows()
        parsed_names = self.sheet.parse_names(rows)
        message = ""
        # message = "| Name" + 7*(" ") + "| Roles | ilvl |\n"
        message += "-"*len(message) + "\n"
        for i, row in enumerate(rows):
            spaces = 10 - len(parsed_names[i])
            rspace = (5 - len(row["Role"])) * " "
            ispace = (4 - len(str(row["Ilvl"]))) * " "
            name_query = f'| {parsed_names[i] + spaces * " "} | {row["Role"] + rspace} | {str(row["Ilvl"]) + ispace} |\n'
            message += name_query

        embed = discord.Embed(title="Bunny Boilers' role and gear list", colour=discord.Colour.blue())
        embed.add_field(name="Name | Role | Ilvl", value=message)
        embed.set_footer(text="to add your gear use >ilvl.add <tank|heal|rdps|mdps> <ilvl>")
        await ctx.send(embed=embed)

def setup(bot):
    bot.add_cog(Ilvl(bot))
