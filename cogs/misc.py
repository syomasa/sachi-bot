import discord
from discord.ext import commands

class Misc(commands.Cog):

    def __init__(self, bot):
        self.bot = bot

    @commands.command(name="datasheet")
    async def send_link(self, ctx):
        LINK = "https://docs.google.com/spreadsheets/d/1KAizBSUuCAp6f62Yw1CH_91Q2sepX6DgwG-IKSiDMVU/edit?usp=sharing"
        await ctx.send(f"Bunny Boiler's datasheet: {LINK}")

def setup(bot):
    bot.add_cog(Misc(bot))
