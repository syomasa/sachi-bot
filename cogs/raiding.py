import discord
from modules.raid import Raid
from discord.ext import commands
from datetime import datetime as dt
from typing import Dict

class RaidingCog(commands.Cog):

    raids: Dict[str, Raid] = {}

    def __init__(self, bot):
        self.bot = bot

    def create_new_raid_embed(self, raid: Raid) -> discord.Embed:
        title = raid.title
        time = raid.time

        # defaults
        tank_msg = "No participants for this role"
        heal_msg = "No participants for this role"
        dps_msg = "No participants for this role"


        if (raid.roles["tank"]): tank_msg = "\n".join(raid.get_name_list("tank"))
        if (raid.roles["heal"]): heal_msg = "\n".join(raid.get_name_list("heal"))
        if (raid.roles["dps"]): dps_msg = "\n".join(raid.get_name_list("dps"))

        embed = discord.Embed(title=title, colour=discord.Colour.red())
        embed.add_field(name="Time", value=f"`{time}`", inline=False)
        embed.add_field(name="Tanks", value=tank_msg, inline=False)
        embed.add_field(name="Healers", value=heal_msg, inline=False)
        embed.add_field(name="Dps", value=dps_msg, inline=False)

        return embed

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction, user):
        if (reaction.me == False):
            return

        msg = reaction.message
        try:
            raid = self.raids[str(msg.id)]

        except KeyError:
            print(f"Couldn't find raid with id {msg.id}. It must be either deleted or missing.")

        else:
            await raid.add_to_roles(msg)

            if (raid.roles["tank"] or raid.roles["heal"] or raid.roles["dps"]):
                new_embed = self.create_new_raid_embed(raid)
                await msg.edit(embed=new_embed)

    @commands.Cog.listener()
    async def on_reaction_remove(self, reaction: discord.Reaction, user: discord.User):
        if (reaction.me == False):
            return

        msg = reaction.message

        try:
            raid = self.raids[str(msg.id)]

        except KeyError:
            print(f"Couldn't find raid with id {msg.id}. It must be either deleted or missing.")

        else:
            # list.remove can be used here due to user being able to change react only on/off
            if (user in raid.roles["tank"] and reaction.emoji == raid.role_icons["tank"]):
                raid.roles["tank"].remove(user)

            if (user in raid.roles["heal"] and reaction.emoji == raid.role_icons["heal"]):
                raid.roles["heal"].remove(user)

            if (user in raid.roles["dps"] and reaction.emoji == raid.role_icons["dps"]):
                raid.roles["dps"].remove(user)

            await msg.edit(embed=self.create_new_raid_embed(raid))

    @commands.command(name="raid.create")
    async def create_raid(self, ctx, title, time):

        if (len(self.raids) >= 3):
            keys = list(self.raids.keys())
            self.raids.pop(keys[0])

        raid = Raid(title, time)
        if (raid.roles["tank"] and raid.roles["heal"] and raid.roles["dps"]):
            tank_msg = "\n".join(raid.roles["tank"])
            heal_msg = "\n".join(raid.roles["heal"])
            dps_msg = "\n".join(raid.roles["dps"])

        else:
            tank_msg = "No participants for this role"
            heal_msg = "No participants for this role"
            dps_msg = "No participants for this role"

        embed = discord.Embed(title=title, colour=discord.Colour.red())
        embed.add_field(name="Time", value=f"`{time}`", inline=False)
        embed.add_field(name="Tanks", value=tank_msg, inline=False)
        embed.add_field(name="Healers", value=heal_msg, inline=False)
        embed.add_field(name="Dps", value=dps_msg, inline=False)

        msg = await ctx.send(embed=embed)
        self.raids.update({str(msg.id): raid})
        await msg.add_reaction(raid.role_icons["tank"])
        await msg.add_reaction(raid.role_icons["heal"])
        await msg.add_reaction(raid.role_icons["dps"])

def setup(bot):
    bot.add_cog(RaidingCog(bot))
