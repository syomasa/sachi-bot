
class Misc:

    @staticmethod
    def bubbleSort(sortableArray):
        for i in range(len(sortableArray)):
            for j in  range(len(sortableArray)-i-1):
                if (sortableArray[j] > sortableArray[j+1]):
                    sortableArray[j], sortableArray[j+1] = sortableArray[j+1], sortableArray[j]

        return sortableArray

if __name__ == "__main__":
    test_array = [23, 32, 2, 12, 68, 3, 37]
    print(Misc.bubbleSort(test_array))
