from datetime import datetime as dt
from typing import List
class Raid():

    roles = {
        "tank": [],
        "heal": [],
        "dps": []
    }

    role_icons = {
        "tank": "🟦",
        "heal": "💚",
        "dps": "🔴"
    }

    def __init__(self, title,
                time = dt.utcnow().strftime("%a %d.%m. %H:%M:%S")):

        self.title = title
        self.time = time

    async def add_to_roles(self, msg):
        """
        adds users to roles based on which reaction they used
            * param msg: Discords message object usually generated when bot sends message
        """

        reactions = msg.reactions

        for reaction in reactions:
            users = await reaction.users().flatten()

            for user in users[:]:
                if (user.bot == True):
                    users.remove(user)

            if (reaction.emoji == self.role_icons["tank"]):
                self.roles["tank"] = users

            elif (reaction.emoji == self.role_icons["heal"]):
                self.roles["heal"] = users

            else:
                self.roles["dps"] = users

    def get_name_list(self, role:str) -> List[str]:
        """
        Used to get list of nicknames in roles, for ease of constructing suitable
        messages for discord.Messages
            * param str role: Used to define which type of list to return options
            are tank, heal or dps
        """

        participant_list = self.roles[role]
        name_list: List[str] = []

        for user in participant_list:
            if (user.nick == None):
                name_list.append(user.name)

            else:
                name_list.append(user.nick)

        return name_list
