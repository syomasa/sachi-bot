import gspread
import os
from dotenv import load_dotenv
from oauth2client.service_account import ServiceAccountCredentials
from pprint import pprint

class Sheet:
    scope = ["https://spreadsheets.google.com/feeds",'https://www.googleapis.com/auth/spreadsheets',
            "https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]
    creds = ServiceAccountCredentials.from_json_keyfile_name(os.getenv("OAUTH_CRED_FILE"), scope)
    client = gspread.authorize(creds)

    def __init__(self, sheet_name, worksheet_name):
        self.sheet = self.client.open(sheet_name).worksheet(worksheet_name)

    def append_datarow(self, data_row):

        col_data = self.sheet.col_values(1) # get value data from column 1
        for i, data in enumerate(col_data[:], 1):
            if data == data_row[0]:
                print('User already exists in database, updating current information instead')
                self.sheet.delete_rows(i)
                self.sheet.insert_row(data_row, i)  # THIS WORKS DONT TOUCH IT VERY IMPORTANT
                print(f'Succesfully updated row {i}')

        if data_row[0] not in col_data:
            self.sheet.append_row(data_row)

    def get_sheet_data(self):
        data_all = self.sheet.get_all_records() # get all data
        row_all = self.sheet.get_all_values() # get all values
        return data_all, row_all

    def get_col_data(self, col_n:int):
        col_data = self.sheet.col_values(col_n)
        return col_data

    def delete_user_information(self, user_id:int):
        data_all, values = self.get_sheet_data()
        for i, data in enumerate(data_all, 1):
            if user_id == int(values[i][0]):
                self.sheet.delete_rows(i+1)

    @staticmethod
    def create_data_row(user_id, user_name, *args):
        """
        Creates row of data containing discord id and name.
            param *args is argument list that are going to be added after id and name
        """
        return [user_id, user_name, *args]

    def parse_names(self, rows, max_length=10):
        """
        Make names short enough for discord
        """
        names = []
        for row in rows:
            if(len(row["Name"]) > max_length):
                row["Name"] = row["Name"][0:10]

            names.append(row["Name"])

        return names

    def sortRows(self):
        scores = {
            "tank": 1,
            "heal": 2,
            "mdps": 3,
            "rdps": 4
        }
        rows, _ = self.get_sheet_data()

        for i in range(len(rows)):
            for j in range(len(rows)-1-i):
                if (scores[rows[j]["Role"]] > scores[rows[j+1]["Role"]]):
                    rows[j], rows[j+1] = rows[j+1], rows[j]

        return rows


if __name__ == '__main__':
    birth = Sheet('Bunny Boilers', 'birthdays')
    raid = Sheet('Bunny Boilers', 'Raiding')
    print(raid.sortRows())
