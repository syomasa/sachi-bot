import discord
from discord.ext import commands
from modules.sheet import Sheet

class BotHelper:

    def __init__(self, bot, sheet):
        self.bot = bot
        self.data = sheet

    def get_user_id(self):
        user_ids = []
        sheet_data, _ = self.data.get_sheet_data()
        for user in sheet_data:
            user_ids.append(user["User_id"])

        return user_ids
